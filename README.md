# Jsonnet Project Template

This project template serves as a starting point to use GitLab CI [Dynamic Child Pipelines](https://docs.gitlab.com/ee/ci/parent_child_pipelines.html#dynamic-child-pipelines) with [Jsonnet](https://jsonnet.org/) to generate complex `gitlab-ci.yml` files at runtime. I furtherly demos the way to combine dynamic generated jsonnet jobs with static pipeline jobs.

Jsonnet provides functions, variables, loops, and conditionals that allow for fully paramaterized YAML configuration. 

# Components

This project contains one '.gitlab/ci' foler and  three files: one file for static pipeline definition, one root .gitlab-ci.yml file and one dynamic defined .gitlab-ci.jsonnet: 

## static files under .gitlab/ci folder:

### deploy.gitlab-ci.yml

This file defines sample deploy jobs for development lifecycle.

## .gitlab-ci.jsonnet

This simple example contains a [Jsonnet script](gitlab-ci.jsonnet) that generates two unique jobs for a scenario where you might want to dynamically generate your testing jobs:

- `rspec`
- `rspec 2`

`local param_job` serves as the template for all the jobs. It also takes advantage of the `parallel` keyword to create two copies of each job.

From here you can use Jsonnet to generate CI YAML in an easy way. To learn more about Jsonnet, check out its [tutorial](https://jsonnet.org/learning/tutorial.html) or [getting started guide](https://jsonnet.org/learning/getting_started.html). The complete [GitLab CI YAML Configuration Reference](https://docs.gitlab.com/ee/ci/yaml/) is also available to help.

## .gitlab-ci.yml

The [`.gitlab-ci.yml`](.gitlab-ci.yml) file contains two important jobs:

### jsonnet-build

This job fetches the Jsonnet package from `apk` and runs it to create the `generated-config.yml`, which is the emitted YAML. The `generated-config.yml` is stored as an artifact to make it available for triggering in the next step.

### trigger-dynamic

`trigger-dynamic` is a [trigger job](https://docs.gitlab.com/ee/ci/yaml/#trigger) responsible for starting the dynamic) child pipeline. It takes the YAML artifact from the previous `jsonnet-build` job, then starts the child pipeline, which is completely dynamically built jobs.

### trigger-hybrid

`trigger-hybrid` is a [trigger job](https://docs.gitlab.com/ee/ci/yaml/#trigger) responsible for starting the hybrid (static and dynamic) child pipeline. It takes the YAML artifact from the previous `jsonnet-build` job and static 'cd-template.yml' file, then starts the child pipeline, which is hybrid of static jobs and dynamic jobs.
